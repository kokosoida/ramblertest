from twisted.internet.protocol import Factory
from twisted.protocols.basic import LineOnlyReceiver
from twisted.internet import reactor
import collections
import redis
import redis.exceptions
import argparse


class Authorizer(object):
    """Provides basic login-password authorization

    Uses redis as login-password storage.
    Highly reliable, safe and durable (not really).

    """
    def __init__(self, redis_conn):
        self.redis_conn = redis_conn
        self.hash_name = "auth"

    def login(self, user, password=None):
        """Checks if user password is correct"""
        try:
            stored_pass = self.redis_conn.hget(self.hash_name, user)
        except redis.exceptions.RedisError:
            return None

        if not stored_pass:
            return True

        if stored_pass == password:
            return True
        return False

    def register(self, user, password):
        """Registers user. Returns false if user already registered"""
        try:
            result = self.redis_conn.hsetnx(self.hash_name, user, password)
        except redis.exceptions.RedisError:
            return None
        return result


class Chat(LineOnlyReceiver):
    def __init__(self, factory):
        self.factory = factory
        self.nickname = 'Anonymous'
        self.active_room = None
        self.joined_rooms = set()

        self.commands = {
            'LOGIN': self.cmd_login,
            'JOIN': self.cmd_join,
            'LEFT': self.cmd_left,
            'REG': self.cmd_reg,
            'CHACT': self.cmd_chact
        }

    def send_message(self, msg):
        """Sends message to client, convering it to bytestring"""
        self.sendLine(bytes(msg, 'utf-8'))

    def connectionMade(self):
        """Called right after user established connection"""
        self.send_message('Hello and welcome!')

    def lineReceived(self, byte_line):
        """Called when user sends a message"""
        try:
            line = byte_line.decode('utf-8')
        except UnicodeDecodeError:
            self.send_message("Can't decode your message. Please use utf-8")
            return

        cmd = None
        try:
            cmd, data = line.split(maxsplit=1)
        except ValueError:
            pass

        if cmd and cmd in self.commands:
            self.commands[cmd](data)
            return

        # We received message from user, send it to active_room
        if not self.active_room:
            self.send_message('Please choose active room by CHACT command')

        message = '[{}] {}: {}'.format(self.active_room, self.nickname, line)
        self.factory.send_broadcast_message(self.active_room, message)
        return

    def connectionLost(self, reason):
        """Called when connection is lost"""
        for room in self.joined_rooms:
            self.factory.rooms[room].remove(self)
            if not self.factory.rooms[room]:
                self.factory.rooms.pop(room, None)

    def cmd_login(self, data):
        """Handler for login command"""
        split_data = data.split(maxsplit=1)
        if not split_data:
            self.send_message("Can't login: no login/password detected")

        if len(split_data) == 1:
            login = split_data[0]
            password = None
        else:
            login = split_data[0]
            password = split_data[1]

        try:
            byte_login = bytes(login, 'ascii')
            if password:
                byte_password = bytes(password, 'ascii')
            else:
                byte_password = None
        except UnicodeEncodeError:
            self.send_message("Please use ascii symbols in login/password")
            return

        login_res = self.factory.authorizer.login(byte_login, byte_password)
        if login_res is None:
            self.send_message("Can't login: redis error")
            return

        if login_res:
            self.nickname = login
        else:
            self.send_message("Can't login: wrong password")
            return

        self.send_message('Logged in as {}'.format(login))

    def cmd_join(self, room):
        """Handler for join room command"""
        self.joined_rooms.add(room)
        self.factory.rooms[room].add(self)
        self.active_room = room
        self.send_message('Joined room {} and set it to active'.format(room))

    def cmd_left(self, room):
        """Handler for left room command"""
        try:
            self.joined_rooms.remove(room)
            self.factory.rooms[room].remove(self)
            self.send_message('You left room: {}'.format(room))
            if not self.factory.rooms[room]:
                self.factory.rooms.pop(room, None)
        except KeyError:
            self.send_message("You're not in room: {}".format(room))

    def cmd_reg(self, data):
        """Handler for registration command"""
        try:
            login, password = data.split(maxsplit=1)
        except ValueError:
            self.send_message("Can't login: no login/password detected")
            return

        try:
            byte_login = bytes(login, 'ascii')
            byte_password = bytes(password, 'ascii')
        except UnicodeEncodeError:
            self.send_message("Please use ascii symbols in login/password")
            return

        reg_result = self.factory.authorizer.register(byte_login, byte_password)
        if reg_result is None:
            self.send_message("Can't register: redis error")
            return

        if reg_result:
            self.send_message('Successfully refistered')
        else:
            self.send_message('User already exists')

    def cmd_chact(self, room):
        """Handler for change active room command"""
        self.active_room = room
        self.send_message('Changed active room to {}'.format(room))


class MultiRoomFactory(Factory):
    def __init__(self, redis_conn):
        self.authorizer = Authorizer(redis_conn)
        self.rooms = collections.defaultdict(set)

    def buildProtocol(self, addr):
            return Chat(self)

    def send_broadcast_message(self, room, message):
        """Send message to all users in room"""
        if room in self.rooms:
            for chat in self.rooms[room]:
                chat.send_message(message)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Chat server')
    parser.add_argument('--rhost', dest='rhost', required=True, help='redis host')
    parser.add_argument('--rport', dest='rport', type=int, required=True, help='redis port')
    parser.add_argument('-p', dest='port', type=int, required=True, help='server port')
    args = parser.parse_args()
    redis_conn = redis.StrictRedis(host=args.rhost, port=args.rport)

    reactor.listenTCP(args.port, MultiRoomFactory(redis_conn))
    reactor.run()


import unittest
import fakeredis
class TransportMock(object):
    def __init__(self):
        self.messages = []

    def writeSequence(self, data):
        line, delimiter = data
        self.messages.append(line)


class ChatTest(unittest.TestCase):
    def setUp(self):
        self.factory = MultiRoomFactory(fakeredis.FakeStrictRedis())
        self.chat1 = self.factory.buildProtocol(None)
        self.chat1.transport = TransportMock()
        self.chat2 = self.factory.buildProtocol(None)
        self.chat2.transport = TransportMock()

    def test_join_left(self):
        self.chat1.lineReceived(b'JOIN 1')
        self.chat1.lineReceived(b'JOIN 2')
        self.chat1.lineReceived(b'QWERTY')
        self.chat1.lineReceived(b'LEFT 1')
        self.chat1.lineReceived(b'LEFT 2')
        self.chat1.lineReceived(b'ABCDEFG')

        self.assertIn(b'[2] Anonymous: QWERTY', self.chat1.transport.messages)
        self.assertNotIn(b'[2] Anonymous: ABCDEFG', self.chat1.transport.messages)
        self.assertNotIn(b'[1] Anonymous: ABCDEFG', self.chat1.transport.messages)
        self.assertIn(b'You left room: 1', self.chat1.transport.messages)
        self.assertIn(b'You left room: 2', self.chat1.transport.messages)

    def test_chat_one_room(self):
        self.chat1.lineReceived(b'JOIN 1')
        self.chat2.lineReceived(b'JOIN 1')
        self.chat1.lineReceived(b'Hello World!')

        self.assertIn(b'[1] Anonymous: Hello World!', self.chat2.transport.messages)

        self.chat1.lineReceived(b'How are you')
        self.chat2.lineReceived(b'Fine')

        self.assertIn(b'[1] Anonymous: How are you', self.chat2.transport.messages)
        self.assertIn(b'[1] Anonymous: Fine', self.chat1.transport.messages)

    def test_chat_many_rooms(self):
        self.chat1.lineReceived(b'JOIN 1')
        self.chat2.lineReceived(b'JOIN 1')
        self.chat2.lineReceived(b'JOIN 2')
        self.chat2.lineReceived(b'JOIN 3')
        self.chat1.lineReceived(b'Hello World 1')

        self.assertIn(b'[1] Anonymous: Hello World 1', self.chat2.transport.messages)

        self.chat1.lineReceived(b'JOIN 2')
        self.chat1.lineReceived(b'Hello World 2')
        self.assertIn(b'[2] Anonymous: Hello World 2', self.chat2.transport.messages)

        self.chat1.lineReceived(b'JOIN 3')
        self.chat1.lineReceived(b'Hello World 3')
        self.assertIn(b'[3] Anonymous: Hello World 3', self.chat2.transport.messages)

    def test_login(self):
        self.chat1.lineReceived(b'LOGIN Vasya')
        self.chat1.lineReceived(b'JOIN 1')
        self.chat1.lineReceived(b'Hello World!')

        self.assertIn(b'[1] Vasya: Hello World!', self.chat1.transport.messages)

    def test_register(self):
        self.chat1.lineReceived(b'REG Oleg pass1')

        self.chat1.lineReceived(b'LOGIN Oleg')
        self.chat2.lineReceived(b'LOGIN Oleg wrongpas')

        self.assertIn(b"Can't login: wrong password", self.chat1.transport.messages)
        self.assertIn(b"Can't login: wrong password", self.chat2.transport.messages)

        self.chat1.lineReceived(b'REG Oleg pass2')
        self.assertIn(b'User already exists', self.chat1.transport.messages)

        self.chat2.lineReceived(b'LOGIN Oleg pass1')
        self.assertIn(b'Logged in as Oleg', self.chat2.transport.messages)

    def test_chact(self):
        self.chat1.lineReceived(b'CHACT 1')
        self.chat2.lineReceived(b'JOIN 1')

        self.chat1.lineReceived(b'ABCDEFG')

        self.assertIn(b'[1] Anonymous: ABCDEFG', self.chat2.transport.messages)
        self.assertNotIn(b'[1] Anonymous: ABCDEFG', self.chat1.transport.messages)

    def test_cyrillic(self):
        self.chat1.lineReceived(b'JOIN 1')
        self.chat1.lineReceived('Привет как дела?'.encode('utf-8'))

        self.assertIn('[1] Anonymous: Привет как дела?'.encode('utf-8'), self.chat1.transport.messages)
