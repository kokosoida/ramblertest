import sys, os
from sys import stdin
import telnetlib
import argparse
import threading
import socket

def telnet_handler(telnet):
    """Reads messages from telnet connection and prints them"""
    try:
        while(1):
            line = telnet.read_until(b'\r\n').strip()
            if not line:
                telnet.close()
                print('Server is down')
                os._exit(0)
            try:
                print(line.decode('utf-8'))
            except UnicodeDecodeError:
                print('Error: undecodable server message')
    except (socket.error, EOFError):
        print('Error connecting to server')
    except KeyboardInterrupt:
        telnet.close()
    os._exit(0)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Chat client')
    parser.add_argument('host', help='chat host')
    parser.add_argument('port', type=int, help='chat port')
    args = parser.parse_args()

    try:
        telnet = telnetlib.Telnet(args.host, args.port)
    except socket.error:
        sys.exit("Can't connect to server")

    t = threading.Thread(target=telnet_handler, args=(telnet,))
    t.start()

    try:
        while(1):
            try:
                line = stdin.readline().strip() + '\r\n'
            except UnicodeDecodeError:
                print("Can't decode your message or it's too long")
                continue
            try:
                telnet.write(line.encode('utf-8'))
            except UnicodeEncodeError:
                print("Can't encode your message, use utf8")
    except (socket.error, AttributeError):
        print('Error connecting to server')
    except KeyboardInterrupt:
        telnet.close()
    os._exit(0)

